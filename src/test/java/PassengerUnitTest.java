
import com.APP2.Main;
import com.google.gson.Gson;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import spark.Spark;
import spark.utils.IOUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static spark.Spark.awaitInitialization;


/**
 * Created by Hubert Wang on 11/22/16.
 */
public class PassengerUnitTest {

    @BeforeClass
    public static void beforeClass()  throws UnknownHostException {
        Main.main(null);
        awaitInitialization();
    }

    @AfterClass
    public static void afterClass() {
        Spark.stop();
    }

    /*********************Passenger Unit Test Start*****************************/
    /**
     * Passenger Info
     */
    String firstName = "Hubert";
    String lastName = "Wang";
    String emailAddress = "hubertwang@cmu.edu";
    String password = "666888";
    String phoneNumber = "6502658759";

    /**
     * Helper function for removing test passenger.
     * */
    private TestResponse removeTestPassenger(String pid) {
        // delete by id
        TestResponse res = request("DELETE", "/v1/passengers/" + pid);
        return res;
    }

    /**
     * Passenger
     * aNewPassengerShouldBeCreated: POST /v1/passengers
     */
    @Test
    public void aNewPassengerShouldBeCreated() {
        TestResponse res = request("POST", "/v1/passengers?"+"firstName="+firstName
                +"&lastName="+lastName+"&emailAddress="+emailAddress
                +"&password="+password+"&phoneNumber="+phoneNumber);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(firstName, json.get("firstName"));
        assertEquals(lastName, json.get("lastName"));
        assertEquals(emailAddress, json.get("emailAddress"));
        assertEquals(phoneNumber, json.get("phoneNumber"));
        assertNotNull(json.get("pid"));

        // Clean up test passenger
        TestResponse delRes = removeTestPassenger(json.get("pid"));
        assertEquals(200, delRes.status);
    }

    /**
     * Passenger
     * allPassengersShouldBeGot: GET /v1/passengers
     */
    @Test
    public void allPassengersShouldBeGot() {
        TestResponse res = request("GET", "/v1/passengers");
        assertEquals(200, res.status);
    }

    /**
     * Passenger
     * aPassengerShouldBeGot: GET /v1/passengers/did
     */
    @Test
    public void aPassengerShouldBeGot() {
        // first, create a new passenger
        TestResponse resNotUse = request("POST", "/v1/passengers?" +
                "firstName=" + firstName + "&lastName=" + lastName +
                "&emailAddress" + "=" + emailAddress + "&password=" +
                password + "&phoneNumber" + "=" + phoneNumber);
        Map<String, String> jsonNotUsed = resNotUse.json();
        String pid = jsonNotUsed.get("pid");
        // second, get by id
        TestResponse res = request("GET", "/v1/passengers/"+pid);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(firstName, json.get("firstName"));
        assertEquals(lastName, json.get("lastName"));
        assertEquals(emailAddress, json.get("emailAddress"));
        assertEquals(phoneNumber, json.get("phoneNumber"));

        // third, get rides of passenger with pid
        TestResponse delRes = removeTestPassenger(pid);
        assertEquals(200, delRes.status);
    }

    /**
     * Passenger
     * ridesShouldBeGotByPassengerId: GET /v1/passengers/pid/rides
     */
    @Test
    public void ridesShouldBeGotByPassengerId() throws UnsupportedEncodingException {
        // first, create a new passenger
        TestResponse resNotUse = request("POST", "/v1/passengers?"
                + "firstName=" + firstName + "&lastName=" + lastName
                + "&emailAddress" + "=" + emailAddress + "&password="
                + password + "&phoneNumber" + "=" + phoneNumber);
        Map<String, String> jsonNotUsed = resNotUse.json();
        String pid = jsonNotUsed.get("pid");
        // second, create a new ride
        String did = "tmp";
        String rideType = "ECONOMY";
        String status = "REQUESTED";
        TestResponse res1 = request("POST", "/v1/rides?"+"pid="+pid+"&did="+did+"&rideType="+rideType+"&status="+status);
        // third, get rides of passenger with pid
        TestResponse res = request("GET", "/v1/passengers/"+pid+"/rides");
        assertEquals(200, res.status);

        // third, get rides of passenger with pid
        TestResponse delRes = removeTestPassenger(pid);
        assertEquals(200, delRes.status);
    }

    /**
     * Passenger
     * aPassengerShouldBeChanged: PUT /v1/passengers/did
     */
    @Test
    public void aPassengerShouldBeChanged() {
        // first, create one for get by id
        TestResponse resNotUse = request("POST", "/v1/passengers?"+"firstName="+firstName+"&lastName="+lastName+"&emailAddress="+emailAddress+"&password="+password+"&phoneNumber="+phoneNumber);
        Map<String, String> jsonNotUsed = resNotUse.json();
        String pid = jsonNotUsed.get("pid");
        // second, get by id
        TestResponse res = request("PUT", "/v1/passengers/"+pid+"?firstName="+"Hubert"+"&lastName="+lastName+"&emailAddress="+emailAddress+"&password="+password+"&phoneNumber="+phoneNumber);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals("Hubert", json.get("firstName"));

        // third, get rides of passenger with pid
        TestResponse delRes = removeTestPassenger(pid);
        assertEquals(200, delRes.status);
    }

    /**
     * Passenger
     * aPassengerShouldBeDeleted: DELETE /v1/passengers/did
     */
    @Test
    public void aPassengerShouldBeDeleted() {
        // first, create one for get by id
        // first, create one for get by id
        TestResponse resNotUse = request("POST", "/v1/passengers?"
                + "firstName=" + firstName + "&lastName=" + lastName
                + "&emailAddress" + "=" + emailAddress+"&password=" + password
                + "&phoneNumber" + "=" + phoneNumber);
        Map<String, String> jsonNotUsed = resNotUse.json();
        String pid = jsonNotUsed.get("did");
        // second, get by id
        TestResponse res = request("DELETE", "/v1/passengers/"+pid);
        assertEquals(200, res.status);
    }
    /*********************Passenger Unit Test End*****************************/

    private TestResponse request(String method, String path) {
        try {
            URL url = new URL("http://localhost:4567" + path);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.setDoOutput(true);
            connection.connect();
            String body = IOUtils.toString(connection.getInputStream());
            return new TestResponse(connection.getResponseCode(), body);
        } catch (IOException e) {
            e.printStackTrace();
            fail("Sending request failed: " + e.getMessage());
            return null;
        }
    }

    private static class TestResponse {

        public final String body;
        public final int status;

        public TestResponse(int status, String body) {
            this.status = status;
            this.body = body;
        }

        public Map<String,String> json() {
            return new Gson().fromJson(body, HashMap.class);
        }
    }
}