import com.APP2.Main;
import com.google.gson.Gson;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import spark.Spark;
import spark.utils.IOUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static spark.Spark.awaitInitialization;

/**
 * Created by Shaojie Cai on 11/25/16.
 */
public class CarUnitTest {

    @BeforeClass
    public static void beforeClass()  throws UnknownHostException {
        Main.main(null);
        awaitInitialization();
    }

    @AfterClass
    public static void afterClass() {
        Spark.stop();
    }

    /**
     * Car
     * aNewCarShouldBeCreated: POST /v1/cars
     * aCarShouldBeGotten: GET /v1/drivers/did
     */
    String cid = "";
    String make = "Audi";
    String model = "A4";
    String license = "12345678";
    String doorCount = "4";

    /**
     * Driver information for test.
     */
    String did = "";
    String firstName = "john";
    String lastName = "Yosh";
    String emailAddress = "john@foobar.com";
    String password = "110";
    String token = "";

    /**
     * Utility function for creating a record of driver for test.
     */
    private String createTestDriver() {
        TestResponse res = request("POST", "/v1/drivers?"+
                "firstName="+firstName+"&lastName="+lastName+
                "&emailAddress="+emailAddress+"&password="+password);
        Map<String, String> json = res.json();
        did = json.get("did");
        return did;
    }

    /**
     * Utility function for removing the reacord of test driver.
     */
    private void removeTestDriver() {
        TestResponse delres = request("DELETE", "/v1/drivers/"+did);
        assertEquals(200, delres.status);
    }

    /**
     * Utility function for creating the record of test car.
     */
    private String createTestCar(String did, String token) {
        TestResponse res = request("POST", "/v1/cars?"
                +"token="+token+"&did="+did+"&make="+make+"&model="
                +model+"&license="+license+"&doorCount="+doorCount);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(make, json.get("make"));
        assertEquals(model, json.get("model"));
        assertEquals(license, json.get("license"));
        assertEquals(doorCount, json.get("doorCount"));
        cid = json.get("cid");
        return cid;
    }

    /**
     * Utility function for removing the record of test car.
     */
    private void removeTestCar() {
        TestResponse delres = request("DELETE", "/v1/cars/"+cid);
        assertEquals(200, delres.status);
    }


    /**
     * Utility function for login the record of test driver.
     */
    private String loginTestDriver() throws UnsupportedEncodingException {
        TestResponse resSession = request("POST", "/v1/sessions?"+
                "emailAddress="+emailAddress+"&password="+password);
        token = resSession.body;
        token = token.substring(1,token.length()-1); // leave out the "" of token string
        token = URLEncoder.encode(token, "UTF-8"); // in case of "+" in token
        return token;
    }
    /**
     * Car
     * aCarShouldBeGotByCarId: GET /v1/cars/:cid
     */
    @Test
    public void aCarShouldBeGotByCarId() throws
            UnsupportedEncodingException {
        // first, create a driver
        String did = createTestDriver();

        // second get token by POST to session
        String token = loginTestDriver();

        // third, create cars for driver
        String cid = createTestCar(did, token);
        assertNotNull(cid);

        // Get car by car id
        TestResponse res = request("GET", "/v1/cars/" + cid);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(cid, json.get("cid"));
        assertEquals(make, json.get("make"));
        assertEquals(model, json.get("model"));
        assertEquals(license, json.get("license"));
        assertEquals(doorCount, json.get("doorCount"));

        // Delete the car
        removeTestCar();

        // Delete the test driver
        removeTestDriver();
    }
    @Test
    public void aDriverShouldBeGotten() throws UnsupportedEncodingException {
        // first, create a driver.
        String did = createTestDriver();

        // second get token by POST to session.
        String token = loginTestDriver();

        // third, create cars for driver.
        String cid = createTestCar(did, token);
        assertNotNull(cid);

        // Get the driver information.
        TestResponse res = request("GET", "/v1/cars/" + cid + "/drivers");
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(did, json.get("did"));
        assertEquals(firstName, json.get("firstName"));
        assertEquals(lastName, json.get("lastName"));
        assertEquals(emailAddress, json.get("emailAddress"));

        // Cleanup: Delete the car.
        removeTestCar();

        // Cleanup: Delete the test driver.
        removeTestDriver();
    }
    @Test
    public void aNewCarShouldBeCreated() throws UnsupportedEncodingException {
        // first, create a driver
        String did = createTestDriver();

        // second get token by POST to session
        String token = loginTestDriver();

        // third, create cars for driver
        String cid = createTestCar(did, token);
        assertNotNull(cid);

        // Cleanup: Delete the car
        removeTestCar();

        // Cleanup: Delete the test driver
        removeTestDriver();
    }
    @Test
    public void aCarShouldBeUpdated() throws UnsupportedEncodingException {
        // first, create a driver
        String did = createTestDriver();

        // second get token by POST to session
        String token = loginTestDriver();

        // third, create cars for driver
        String cid = createTestCar(did, token);
        assertNotNull(cid);

        // Update car information.
        String newmake = "BMW";
        String newmodel = "BMWseries3";
        String newlicense = "abcdefgh";
        String newdoorCount = "6";
        TestResponse res = request("PUT", "/v1/cars/"+cid+"?"
                +"token="+token +"&did="+did+"&make=" + newmake  +"&model="
                +newmodel+"&license=" + newlicense+ "&doorCount="+
                newdoorCount);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(newmake, json.get("make"));
        assertEquals(newmodel, json.get("model"));
        assertEquals(newlicense, json.get("license"));
        assertEquals(newdoorCount, json.get("doorCount"));
        assertEquals(cid, json.get("cid"));

        // Cleanup: Delete the car
        removeTestCar();

        // Cleanup: Delete the test driver
        removeTestDriver();
    }
    /**
     * Driver
     * allDriversShouldBeGot: GET /v1/drivers
     */
    @Test
    public void allCarsShouldBeGot() {
        TestResponse res = request("GET", "/v1/cars");
        assertEquals(200, res.status);
    }
    /**
     * Driver
     * allDriversShouldBeGot: GET /v1/drivers
     */
    @Test
    public void aCarsShouldBeDeleted() throws UnsupportedEncodingException {
        // first, create a driver
        String did = createTestDriver();

        // second get token by POST to session
        String token = loginTestDriver();

        // third, create cars for driver
        String cid = createTestCar(did, token);
        assertNotNull(cid);

        // Delete the car
        TestResponse delres = request("DELETE", "/v1/cars/"+cid);
        assertEquals(200, delres.status);

        // Delete the test driver
        removeTestDriver();
    }


    private TestResponse request(String method, String path) {
        try {
            URL url = new URL("http://localhost:4567" + path);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.setDoOutput(true);
            connection.connect();
            String body = IOUtils.toString(connection.getInputStream());
            return new TestResponse(connection.getResponseCode(), body);
        } catch (IOException e) {
            e.printStackTrace();
            fail("Sending request failed: " + e.getMessage());
            return null;
        }
    }

    private static class TestResponse {

        public final String body;
        public final int status;

        public TestResponse(int status, String body) {
            this.status = status;
            this.body = body;
        }

        public Map<String,String> json() {
            return new Gson().fromJson(body, HashMap.class);
        }
    }
}