
import com.APP2.Main;
import com.google.gson.Gson;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import spark.Spark;
import spark.utils.IOUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static spark.Spark.awaitInitialization;


/**
 * Created by Hubert Wang on 11/22/16.
 */
public class SessionUnitTest {

    @BeforeClass
    public static void beforeClass()  throws UnknownHostException {
        Main.main(null);
        awaitInitialization();
    }

    @AfterClass
    public static void afterClass() {
        Spark.stop();
    }

    /*********************Session Unit Test Start*****************************/
    /**
     * Driver Info for testing session
     */
    String firstName = "john";
    String lastName = "Yosh";
    String emailAddress = "john@foobar.com";
    String password = "110";

    /**
     * Helper function for removing test driver.
     * */
    private TestResponse removeTestDriver(String did) {
        // delete by id
        TestResponse res = request("DELETE", "/v1/drivers/" + did);
        return res;
    }

    /**
     * Session
     * aNewTokenShouldBeGot: POST /v1/sessions
     */
    @Test
    public void aNewTokenShouldBeGot() {
        TestResponse res = request("POST", "/v1/drivers?"+"firstName="+firstName+"&lastName="+lastName+"&emailAddress="+emailAddress+"&password="+password);
        Map<String, String> json = res.json();
        
        TestResponse resSession = request("POST", "/v1/sessions?"+"emailAddress="+emailAddress+"&password="+password);
        String token = resSession.body;
        assertEquals(200, res.status);
        assertNotNull(token);

        // Clean up test driver
        TestResponse delRes = removeTestDriver(json.get("did"));
        assertEquals(200, delRes.status);
    }

    /**
     * Driver
     * validateTokenByCreateCar: POST /v1/sessions, POST /v1/drivers/did/cars
     */
    @Test
    public void aNewCarShouldBeCreatedByDriverId() throws UnsupportedEncodingException {
        // first, create a new driver
        TestResponse resNotUse = request("POST", "/v1/drivers?"+"firstName="+firstName+"&lastName="+lastName+"&emailAddress="+emailAddress+"&password="+password);
        Map<String, String> jsonNotUsed = resNotUse.json();
        String did = jsonNotUsed.get("did");
        // second get token by POST to session
        TestResponse resSession = request("POST", "/v1/sessions?"+"emailAddress="+emailAddress+"&password="+password);
        String token = resSession.body;
        token = token.substring(1,token.length()-1); // leave out the "" of token string
        token = URLEncoder.encode(token, "UTF-8"); // in case of "+" in token
        // third, post a new car to driver with did and token
        String make = "BMW";
        String model = "BMWseries3";
        String license = "abcdefgh";
        String doorCount = "4";
        TestResponse res = request("POST", "/v1/drivers/"+did+"/cars?"+"token="+token+"&did="+did+"&make="+make+"&model="+model+"&license="+license+"&doorCount="+doorCount);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(did, json.get("did"));
        assertEquals(make, json.get("make"));
        assertEquals(model, json.get("model"));
        assertEquals(license, json.get("license"));
        assertEquals(doorCount, json.get("doorCount"));
        assertNotNull(json.get("cid"));

        // Clean up test driver
        TestResponse delRes = removeTestDriver(json.get("did"));
        assertEquals(200, delRes.status);
    }
    /*********************Session Unit Test End*****************************/

    private TestResponse request(String method, String path) {
        try {
            URL url = new URL("http://localhost:4567" + path);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.setDoOutput(true);
            connection.connect();
            String body = IOUtils.toString(connection.getInputStream());
            return new TestResponse(connection.getResponseCode(), body);
        } catch (IOException e) {
            e.printStackTrace();
            fail("Sending request failed: " + e.getMessage());
            return null;
        }
    }

    private static class TestResponse {

        public final String body;
        public final int status;

        public TestResponse(int status, String body) {
            this.status = status;
            this.body = body;
        }

        public Map<String,String> json() {
            return new Gson().fromJson(body, HashMap.class);
        }
    }
}