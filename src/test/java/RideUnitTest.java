import com.APP2.Main;
import com.google.gson.Gson;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import spark.Spark;
import spark.utils.IOUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static spark.Spark.awaitInitialization;

/**
 * Created by shaojiecai on 11/26/16.
 */
public class RideUnitTest {

    @BeforeClass
    public static void beforeClass()  throws UnknownHostException {
        Main.main(null);
        awaitInitialization();
    }

    @AfterClass
    public static void afterClass() {
        Spark.stop();
    }

    /**
     * Car
     * aNewCarShouldBeCreated: POST /v1/cars
     * aCarShouldBeGotten: GET /v1/drivers/did
     */
    String cid = "";
    String make = "Audi";
    String model = "A4";
    String license = "12345678";
    String doorCount = "4";

    /**
     * Driver information for test.
     */
    String did = "";
    String dfirstName = "john";
    String dlastName = "Yosh";
    String demailAddress = "john@foobar.com";
    String dpassword = "110";
    String dtoken = "";

    /**
     * Passenger Info
     */
    String pid = "";
    String firstName = "Shaojie";
    String lastName = "Cai";
    String emailAddress = "tina@cmu.edu";
    String password = "123";
    String phoneNumber = "5105103456";
    String ptoken = "";

    /**
     * Utility function for creating a reacord of driver for test.
     */
    private String createTestDriver() {
        TestResponse res = request("POST", "/v1/drivers?"+
                "firstName="+dfirstName+"&lastName="+dlastName+
                "&emailAddress="+demailAddress+"&password="+dpassword);
        Map<String, String> json = res.json();
        did = json.get("did");
        return did;
    }

    /**
     * Utility function for removing the reacord of test driver.
     */
    private void removeTestDriver(String did) {
        TestResponse delres = request("DELETE", "/v1/drivers/"+did);
        assertEquals(200, delres.status);
    }

    /**
     * Utility function for creating the record of test car.
     */
    private String createTestCar(String did, String token) {
        TestResponse res = request("POST", "/v1/cars?"
                +"token="+token+"&did="+did+"&make="+make+"&model="
                +model+"&license="+license+"&doorCount="+doorCount);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(make, json.get("make"));
        assertEquals(model, json.get("model"));
        assertEquals(license, json.get("license"));
        assertEquals(doorCount, json.get("doorCount"));
        cid = json.get("cid");
        return cid;
    }

    /**
     * Utility function for removing the record of test car.
     */
    private void removeTestCar(String cid) {
        TestResponse delres = request("DELETE", "/v1/cars/"+cid);
        assertEquals(200, delres.status);
    }

    /**
     * Utility function for login the record of test driver.
     */
    private String loginTestDriver() throws UnsupportedEncodingException {
        TestResponse resSession = request("POST", "/v1/sessions?"+
                "emailAddress="+demailAddress+"&password="+dpassword);
        String token = resSession.body;
        token = token.substring(1,token.length()-1); // leave out the "" of token string
        token = URLEncoder.encode(token, "UTF-8"); // in case of "+" in token
        dtoken = token;
        return token;
    }

    /**
     * Utility function for login the record of test driver.
     */
    private String loginTestPassenger() throws UnsupportedEncodingException {
        TestResponse resSession = request("POST", "/v1/sessions?"+
                "emailAddress="+emailAddress+"&password="+password);
        String token = resSession.body;
        token = token.substring(1,token.length()-1); // leave out the "" of token string
        token = URLEncoder.encode(token, "UTF-8"); // in case of "+" in token
        ptoken = token;
        return token;
    }

    /**
     * Utility function for creating the record of passenger.
     */
    private String createTestPassenger() {
        TestResponse res = request("POST", "/v1/passengers?"+"firstName="+firstName+"&lastName="+lastName+"&emailAddress="+emailAddress+"&password="+password+"&phoneNumber="+phoneNumber);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(firstName, json.get("firstName"));
        assertEquals(lastName, json.get("lastName"));
        assertEquals(emailAddress, json.get("emailAddress"));
        assertEquals(phoneNumber, json.get("phoneNumber"));
        assertNotNull(json.get("pid"));
        pid = json.get("pid");
        return pid;
    }

    /**
     * Utility function for removing the record of passenger.
     */
    private void removeTestPassenger(String pid) {
        TestResponse res = request("DELETE", "/v1/passengers/"+pid);
        assertEquals(200, res.status);
    }

    /**
     * Utility function for creating a ride.
     */
    private String createRide(String pid, String did, String rideType, String
            status, String token) {
        TestResponse res = request("POST", "/v1/rides?" + "pid=" +
                pid +"&did=" + did + "&rideType=" + rideType +
                "&status=" + status + "&token=" + token);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(pid, json.get("pid"));
        assertEquals(json.get("did"), did);
        assertEquals(rideType, json.get("rideType"));
        assertEquals(status, json.get("status"));
        assertNotNull(json.get("rid"));
        return json.get("rid");
    }

    /**
     * Utility function for removing the record of passenger.
     */
    private void removeRide(String rid) {
        TestResponse res = request("DELETE", "/v1/rides/"+rid);
        assertEquals(200, res.status);
    }

    /**
     * Utility function for adding a route point.
     */
    private void addRoutePoint(String rid, String lati, String longi) {
        // Post a route point
        TestResponse res = request("POST", "/v1/rides/" + rid +
                "/routePoints?" + "lati=" + lati +"&longi=" + longi);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(rid, json.get("rid"));
        assertEquals(lati, json.get("lati"));
        assertEquals(longi, json.get("longi"));
    }

    /**
     * Ride
     * aNewRideShouldBeRequested: POST /v1/rides
     */
    @Test
    public void aNewRideShouldBeRequested() throws
            UnsupportedEncodingException {

        // create a driver
        String did = createTestDriver();

        // create a passenger
        String pid = createTestPassenger();
        assertNotNull(pid);

        // get token by POST to session
//        String token = loginTestDriver();

        // create cars for driver
//        String cid = createTestCar(did, dtoken);
//        assertNotNull(cid);

        // login ride
        String token = loginTestPassenger();

        String rideType = "ECONOMY";
        String status = "REQUESTED";

        TestResponse res = request("POST", "/v1/rides?" + "pid=" +
                pid +"&did=" + did + "&rideType=" + rideType +
                "&status=" + status + "&token=" + token);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(pid, json.get("pid"));
        assertEquals(json.get("did"), did);
        assertEquals(rideType, json.get("rideType"));
        assertEquals(status, json.get("status"));
        assertNotNull(json.get("rid"));
        String rid = json.get("rid");

        // Cleanup: Remove test ride
        removeRide(rid);

        // Cleanup: Remove test driver
        removeTestDriver(did);

        // Cleanup: Remove test passenger
        removeTestPassenger(pid);
    }

    /**
     * Ride
     * aNewRoutePointShouldBeCreate: POST /v1/rides
     */
    @Test
    public void aNewRoutePointShouldBeCreate() throws
            UnsupportedEncodingException {
        // create a driver
        String did = createTestDriver();

        // create a passenger
        String pid = createTestPassenger();
        assertNotNull(pid);

        // login ride
        String token = loginTestPassenger();

        String rideType = "ECONOMY";
        String status = "REQUESTED";

        // Create a ride
        String rid = createRide(pid, did, rideType, status, token);

        // Post a route point
        String lati = "10";
        String longi = "20";
        TestResponse res = request("POST", "/v1/rides/" + rid +
                "/routePoints?" + "lati=" + lati +"&longi=" + longi);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(rid, json.get("rid"));
        assertEquals(lati, json.get("lati"));
        assertEquals(longi, json.get("longi"));

        // Cleanup: Remove test ride
        removeRide(rid);

        // Cleanup: Remove test driver
        removeTestDriver(did);

        // Cleanup: Remove test passenger
        removeTestPassenger(pid);
    }

    /**
     * Ride
     * allRoutePointsShouldBeGot: GET /rides/:rideId/routePoints
     */
    @Test
    public void allRoutePointsShouldBeGot() throws
            UnsupportedEncodingException {
        // create a driver
        String did = createTestDriver();

        // create a passenger
        String pid = createTestPassenger();
        assertNotNull(pid);

        // login ride
        String token = loginTestPassenger();

        String rideType = "ECONOMY";
        String status = "REQUESTED";

        // Create a ride
        String rid = createRide(pid, did, rideType, status, token);

        // Add a route point
        String lati = "11";
        String longi = "21";
        addRoutePoint(rid, lati, longi);

        String lati2 = "12";
        String longi2 = "22";
        addRoutePoint(rid, lati2, longi2);

        TestResponse res = request("GET", "/v1/rides/" + rid + "/routePoints");
        Map<String, String>[] json = res.jsonArray();
        assertEquals(200, res.status);
        assertEquals(rid, json[0].get("rid"));
        assertEquals(lati, json[0].get("lati"));
        assertEquals(longi, json[0].get("longi"));

        assertEquals(rid, json[1].get("rid"));
        assertEquals(lati2, json[1].get("lati"));
        assertEquals(longi2, json[1].get("longi"));

        // Cleanup: Remove test ride
        removeRide(rid);

        // Cleanup: Remove test driver
        removeTestDriver(did);

        // Cleanup: Remove test passenger
        removeTestPassenger(pid);
    }

    /**
     * Ride
     * aLatestRoutePointShouldBeGot: GET /v1/rides/:rid/routePoints/latest
     */
    @Test
    public void aLatestRoutePointShouldBeGot() throws
            UnsupportedEncodingException {
        // create a driver
        String did = createTestDriver();

        // create a passenger
        String pid = createTestPassenger();
        assertNotNull(pid);

        // login ride
        String token = loginTestPassenger();

        String rideType = "ECONOMY";
        String status = "REQUESTED";

        // Create a ride
        String rid = createRide(pid, did, rideType, status, token);

        // Add a route point
        String lati = "21";
        String longi = "31";
        addRoutePoint(rid, lati, longi);

        String lati2 = "22";
        String longi2 = "32";
        addRoutePoint(rid, lati2, longi2);

        TestResponse res = request("GET", "/v1/rides/" + rid +
                "/routePoints/latest");
        Map<String, String> json = res.json();
        assertEquals(200, res.status);

        assertEquals(rid, json.get("rid"));
        assertEquals(lati2, json.get("lati"));
        assertEquals(longi2, json.get("longi"));

        // Cleanup: Remove test ride
        removeRide(rid);

        // Cleanup: Remove test driver
        removeTestDriver(did);

        // Cleanup: Remove test passenger
        removeTestPassenger(pid);
    }

    /**
     * Ride
     * aRideShouldBeGotById: GET /v1/rides/:rid
     */
    @Test
    public void aRideShouldBeGotById() throws
            UnsupportedEncodingException {
        // create a driver
        String did = createTestDriver();

        // create a passenger
        String pid = createTestPassenger();
        assertNotNull(pid);

        // login ride
        String token = loginTestPassenger();

        String rideType = "ECONOMY";
        String status = "REQUESTED";

        // Create a ride
        String rid = createRide(pid, did, rideType, status, token);

        TestResponse res = request("GET", "/v1/rides/" + rid);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(rid, json.get("rid"));
        assertEquals(rideType, json.get("rideType"));
        assertEquals(status, json.get("status"));

        // Cleanup: Remove test ride
        removeRide(rid);

        // Cleanup: Remove test driver
        removeTestDriver(did);

        // Cleanup: Remove test passenger
        removeTestPassenger(pid);
    }

    /**
     * Ride
     * allRidesShouldBeGot: GET /v1/rides
     */
    @Test
    public void allRidesShouldBeGot() throws
            UnsupportedEncodingException {
        TestResponse res = request("GET", "/v1/rides");
        Map<String, String>[] json = res.jsonArray();
        assertEquals(200, res.status);
    }

    /**
     * Ride
     * aRidesShouldBeUpdated: PUT /v1/rides/:rid
     */
    @Test
    public void aRidesShouldBeUpdated() throws
            UnsupportedEncodingException {
        // create a driver
        String did = createTestDriver();

        // create a passenger
        String pid = createTestPassenger();
        assertNotNull(pid);

        // login ride
        String token = loginTestPassenger();

        String rideType = "ECONOMY";
        String status = "REQUESTED";

        // Create a ride
        String rid = createRide(pid, did, rideType, status, token);

        String[] testTypes = {"ECONOMY", "PREMIUM", "EXECUTIVE"};
        String[] testStatus = {"REQUESTED", "AWAITING_DRIVER",
                "DRIVE_ASSIGNED", "IN_PROGRESS", "ARRIVED", "CLOSED"};
        for(String newType: testTypes) {
            TestResponse res = request("PUT", "/v1/rides/" + rid + "?" + "pid=" +
                    pid + "&did=" + did + "&rideType=" + newType +
                    "&status=" + status);
            Map<String, String> json = res.json();
            assertEquals(200, res.status);
            assertEquals(rid, json.get("rid"));
            assertEquals(newType, json.get("rideType"));
            assertEquals(status, json.get("status"));
        }

        for(String newStatus: testStatus) {
            TestResponse res = request("PUT", "/v1/rides/" + rid + "?" + "pid=" +
                    pid + "&did=" + did + "&rideType=" + rideType +
                    "&status=" + newStatus);
            Map<String, String> json = res.json();
            assertEquals(200, res.status);
            assertEquals(rid, json.get("rid"));
            assertEquals(rideType, json.get("rideType"));
            assertEquals(newStatus, json.get("status"));
        }

        // Cleanup: Remove test ride
        removeRide(rid);

        // Cleanup: Remove test driver
        removeTestDriver(did);

        // Cleanup: Remove test passenger
        removeTestPassenger(pid);
    }

    /**
     * Ride
     * aRidesShouldBeDeleted: DELETE /v1/rides/:rid
     */
    @Test
    public void aRidesShouldBeDeleted() throws
            UnsupportedEncodingException {
        // create a driver
        String did = createTestDriver();

        // create a passenger
        String pid = createTestPassenger();
        assertNotNull(pid);

        // login ride
        String token = loginTestPassenger();

        String rideType = "ECONOMY";
        String status = "REQUESTED";

        // Create a ride
        String rid = createRide(pid, did, rideType, status, token);


        // Remove test ride
        TestResponse res = request("DELETE", "/v1/rides/"+rid);
        assertEquals(200, res.status);

        // Post position on ride
        TestResponse resDel = request("POST", "/v1/rides?" + "pid=" +
                pid +"&did=" + did + "&rideType=" + rideType +
                "&status=" + status + "&token=" + token);
        Map<String, String> json = resDel.json();
        assertEquals(200, resDel.status);

        // Cleanup: Remove test driver
        removeTestDriver(did);

        // Cleanup: Remove test passenger
        removeTestPassenger(pid);
    }


    private TestResponse request(String method, String path) {
        try {
            URL url = new URL("http://localhost:4567" + path);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.setDoOutput(true);
            connection.connect();
            String body = IOUtils.toString(connection.getInputStream());
            return new TestResponse(connection.getResponseCode(), body);
        } catch (IOException e) {
            e.printStackTrace();
            fail("Sending request failed: " + e.getMessage());
            return null;
        }
    }

    private static class TestResponse {

        public final String body;
        public final int status;

        public TestResponse(int status, String body) {
            this.status = status;
            this.body = body;
        }

        public Map<String,String> json() {
            return new Gson().fromJson(body, HashMap.class);
        }

        public Map<String,String>[] jsonArray() {
            return new Gson().fromJson(body, HashMap[].class);
        }
    }
}
