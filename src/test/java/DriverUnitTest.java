
import com.APP2.Main;
import com.google.gson.Gson;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import spark.Spark;
import spark.utils.IOUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static spark.Spark.awaitInitialization;


/**
 * Created by Hubert Wang on 11/22/16.
 */
public class DriverUnitTest {

    @BeforeClass
    public static void beforeClass()  throws UnknownHostException {
        Main.main(null);
        awaitInitialization();
    }

    @AfterClass
    public static void afterClass() {
        Spark.stop();
    }

    /*********************Driver Unit Test Start*****************************/
    /**
     * Driver Info
     */
    String firstName = "john";
    String lastName = "Yosh";
    String emailAddress = "john@foobar.com";
    String password = "110";

    /**
     * Helper function for removing test driver.
     * */
    private TestResponse removeTestDriver(String did) {
        // delete by id
        TestResponse res = request("DELETE", "/v1/drivers/" + did);
        return res;
    }
    /**
     * Driver
     * aNewDriverShouldBeCreated: POST /v1/drivers
     */
    @Test
    public void aNewDriverShouldBeCreated() {
        TestResponse res = request("POST", "/v1/drivers?"+"firstName="
                +firstName+"&lastName="+lastName+"&emailAddress="+
                emailAddress+"&password="+password);//"/v1/drivers?firstName=john&lastName=Yosh&emailAddress=john@foobar.com&password=110");
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(firstName, json.get("firstName"));
        assertEquals(emailAddress, json.get("emailAddress"));
        assertNotNull(json.get("did"));

        // Clean up test driver
        TestResponse delRes = removeTestDriver(json.get("did"));
        assertEquals(200, delRes.status);
    }

    /**
     * Driver
     * aNewCarShouldBeCreatedByDriverId: POST /v1/drivers/did/cars
     */
    @Test
    public void aNewCarShouldBeCreatedByDriverId() throws UnsupportedEncodingException {
        // first, create a new driver
        TestResponse resNotUse = request("POST", "/v1/drivers?"+"firstName="+firstName+"&lastName="+lastName+"&emailAddress="+emailAddress+"&password="+password);
        Map<String, String> jsonNotUsed = resNotUse.json();
        String did = jsonNotUsed.get("did");
        // second get token by POST to session
        TestResponse resSession = request("POST", "/v1/sessions?"+"emailAddress="+emailAddress+"&password="+password);
        String token = resSession.body;
        token = token.substring(1,token.length()-1); // leave out the "" of token string
        token = URLEncoder.encode(token, "UTF-8"); // in case of "+" in token
        // third, post a new car to driver with did and token
        String make = "BMW";
        String model = "BMWseries3";
        String license = "abcdefgh";
        String doorCount = "4";
        TestResponse res = request("POST", "/v1/drivers/"+did+"/cars?"+"token="+token+"&did="+did+"&make="+make+"&model="+model+"&license="+license+"&doorCount="+doorCount);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(did, json.get("did"));
        assertEquals(make, json.get("make"));
        assertEquals(model, json.get("model"));
        assertEquals(license, json.get("license"));
        assertEquals(doorCount, json.get("doorCount"));
        assertNotNull(json.get("cid"));

        // Clean up test driver
        TestResponse delRes = removeTestDriver(did);
        assertEquals(200, delRes.status);
    }

    /**
     * Driver
     * allDriversShouldBeGot: GET /v1/drivers
     */
    @Test
    public void allDriversShouldBeGot() {
        TestResponse res = request("GET", "/v1/drivers");
        assertEquals(200, res.status);
    }

    /**
     * Driver
     * aDriverShouldBeGot: GET /v1/drivers/did
     */
    @Test
    public void aDriverShouldBeGot() {
        // first, create one for get by id
        TestResponse resNotUse = request("POST", "/v1/drivers?"+"firstName="+firstName+"&lastName="+lastName+"&emailAddress="+emailAddress+"&password="+password);
        Map<String, String> jsonNotUsed = resNotUse.json();
        String did = jsonNotUsed.get("did");
        // second, get by id
        TestResponse res = request("GET", "/v1/drivers/"+did);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals(firstName, json.get("firstName"));
        assertEquals(lastName, json.get("lastName"));
        assertEquals(emailAddress, json.get("emailAddress"));

        // Clean up test driver
        TestResponse delRes = removeTestDriver(did);
        assertEquals(200, delRes.status);
    }

    /**
     * Driver
     * carsShouldBeGotByDriverId: GET /v1/drivers/did/cars
     */
    @Test
    public void carsShouldBeGotByDriverId() throws UnsupportedEncodingException {
        // first, create a driver
        TestResponse resNotUse = request("POST", "/v1/drivers?"+"firstName="+firstName+"&lastName="+lastName+"&emailAddress="+emailAddress+"&password="+password);
        Map<String, String> jsonNotUsed = resNotUse.json();
        String did = jsonNotUsed.get("did");
        // second get token by POST to session
        TestResponse resSession = request("POST", "/v1/sessions?"+"emailAddress="+emailAddress+"&password="+password);
        String token = resSession.body;
        token = token.substring(1,token.length()-1); // leave out the "" of token string
        token = URLEncoder.encode(token, "UTF-8"); // in case of "+" in token
        // third, create cars for driver
        String make = "BMW";
        String model = "BMWseries3";
        String license = "abcdefgh";
        String doorCount = "4";
        TestResponse res1 = request("POST", "/v1/drivers/"+did+"/cars?"+"token="+token+"&did="+did+"&make="+make+"&model="+model+"&license="+license+"&doorCount="+doorCount);
        // fourth, get cars of driver with did
        TestResponse res = request("GET", "/v1/drivers/"+did+"/cars");
        assertEquals(200, res.status);

        // Clean up test driver
        TestResponse delRes = removeTestDriver(did);
        assertEquals(200, delRes.status);
    }

    /**
     * Driver
     * aDriverShouldBeChanged: PUT /v1/drivers/did
     */
    @Test
    public void aDriverShouldBeChanged() {
        // first, create one for get by id
        TestResponse resNotUse = request("POST", "/v1/drivers?"+"firstName="+firstName+"&lastName="+lastName+"&emailAddress="+emailAddress+"&password="+password);
        Map<String, String> jsonNotUsed = resNotUse.json();
        String did = jsonNotUsed.get("did");
        // second, get by id
        TestResponse res = request("PUT", "/v1/drivers/"+did+"?firstName="+"Hubert"+"&lastName="+lastName+"&emailAddress="+emailAddress+"&password="+password);
        Map<String, String> json = res.json();
        assertEquals(200, res.status);
        assertEquals("Hubert", json.get("firstName"));

        // Clean up test driver
        TestResponse delRes = removeTestDriver(did);
        assertEquals(200, delRes.status);
    }

    /**
     * Driver
     * aDriverShouldBeDeleted: DELETE /v1/drivers/did
     */
    @Test
    public void aDriverShouldBeDeleted() {
        // first, create one for get by id
        TestResponse resNotUse = request("POST", "/v1/drivers?"+"firstName="+firstName+"&lastName="+lastName+"&emailAddress="+emailAddress+"&password="+password);
        Map<String, String> jsonNotUsed = resNotUse.json();
        String did = jsonNotUsed.get("did");
        // second, get by id
        TestResponse res = request("DELETE", "/v1/drivers/"+did);
        assertEquals(200, res.status);
    }
    /*********************Driver Unit Test End*****************************/

    private TestResponse request(String method, String path) {
        try {
            URL url = new URL("http://localhost:4567" + path);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.setDoOutput(true);
            connection.connect();
            String body = IOUtils.toString(connection.getInputStream());
            return new TestResponse(connection.getResponseCode(), body);
        } catch (IOException e) {
            e.printStackTrace();
            fail("Sending request failed: " + e.getMessage());
            return null;
        }
    }

    private static class TestResponse {

        public final String body;
        public final int status;

        public TestResponse(int status, String body) {
            this.status = status;
            this.body = body;
        }

        public Map<String,String> json() {
            return new Gson().fromJson(body, HashMap.class);
        }
    }
}