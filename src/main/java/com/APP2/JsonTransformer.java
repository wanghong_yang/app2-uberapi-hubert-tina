package com.APP2;
/**
 * Created by Hubert on 11/2/16.
 */
import com.google.gson.Gson;
import spark.ResponseTransformer;

/**
 * Utility class for transforming object to JSON string.
 */
public class JsonTransformer implements ResponseTransformer {
    private Gson gson = new Gson();

    @Override
    public String render(Object model) {
        return gson.toJson(model);
    }

}
