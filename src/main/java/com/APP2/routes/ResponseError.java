package com.APP2.routes;

/**
 * Created by Hubert on 11/2/16.
 */

/*
 * Response error
 * usage: new ResponseError("msg here")
 */
public class ResponseError {
    private String message;

    public ResponseError(String message, String... args) {
        this.message = String.format(message, args);
    }

    public ResponseError(Exception e) {
        this.message = e.getMessage();
    }

    public String getMessage() {
        return this.message;
    }
}
