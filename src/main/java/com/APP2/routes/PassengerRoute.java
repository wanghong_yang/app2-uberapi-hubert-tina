package com.APP2.routes;

import com.mongodb.*;
import com.APP2.models.Passenger;
import com.APP2.models.Ride;
import com.APP2.JsonTransformer;

import java.util.ArrayList;
import java.util.List;

import static spark.Spark.*;
import static spark.Spark.after;

/**
 * Controller class that install route of Passenger related request handler.
 * It depends on the CRUD implementation of Passenger Entity.
 */
public class PassengerRoute {
    public PassengerRoute() {
        // post a passenger
        post("/v1/passengers", (req, res) -> {
            // Error Handling
            failIfInvalid(req.queryParams("firstName"),
                    req.queryParams("lastName"),
                    req.queryParams("emailAddress"),
                    req.queryParams("password"),
                    req.queryParams("phoneNumber"));

            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection passengerCol = db.getCollection("passengers");

            // Check if the passenger is exist.
            List<Passenger> existPassengers = queryPassengerByEmail
                    (passengerCol, req.queryParams("emailAddress"));
            if (existPassengers.size() > 0) {
                throw new IllegalArgumentException("Error Code: 1100\n" +
                        "Error Msg: Parameter 'emailAddress' already exist.\n" +
                        "Status Code: 400");
            }

            // push a new Passenger to database
            Passenger passenger = new Passenger(req.queryParams("firstName"),
                    req.queryParams("lastName"),
                    req.queryParams("emailAddress"),
                    req.queryParams("password"),
                    req.queryParams("phoneNumber"));
            DBObject doc = createDBObject(passenger);
            WriteResult result = passengerCol.insert(doc);

            //close resources
            mongo.close();

            return passenger;
        }, new JsonTransformer());

        // get a Passenger by id
        get("/v1/passengers/:pid", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection passengerCol = db.getCollection("passengers");

            // read passenger by id
            String pid = req.params(":pid");
            DBObject query = BasicDBObjectBuilder.start().add("pid", pid).get();
            DBCursor cursor = passengerCol.find(query);
            while(cursor.hasNext()) {
                DBObject Obj = cursor.next();
                return convertDBObject(Obj);
            }

            //close resources
            mongo.close();

            // error handling: no passenger with corresponding id found
            res.status(400);
            return new ResponseError("No Passenger with passenger id '%s' found", pid);
        }, new JsonTransformer());

        // GET /passegers/:passengerId/rides, get passenger's rides by passenger id
        get("/v1/passengers/:pid/rides", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection RideCol = db.getCollection("rides");

            // read passenger by id
            List<Ride> rides = new ArrayList<Ride>();
            String pid = req.params(":pid");
            DBObject query = BasicDBObjectBuilder.start().add("pid", pid).get();
            DBCursor cursor = RideCol.find(query);
            while(cursor.hasNext()) {
                DBObject Obj = cursor.next();
                rides.add(convertRideDBObject(Obj));
            }

            //close resources
            mongo.close();

            return rides;
        }, new JsonTransformer());

        // get passengers
        get("/v1/passengers", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection passengerCol = db.getCollection("passengers");

            // read passengers
            List<Passenger> passengers = new ArrayList<Passenger>();
            DBCursor cursor = passengerCol.find();
            while(cursor.hasNext()) {
                DBObject Obj = cursor.next();
                passengers.add(convertDBObject(Obj));
            }

            //close resources
            mongo.close();

            return passengers;
        }, new JsonTransformer());

        // update a passenger
        put("/v1/passengers/:pid", (req, res) -> {
            // Error Handling
            failIfInvalid(req.queryParams("firstName"), req.queryParams("lastName"), req.queryParams("emailAddress"), req.queryParams("password"), req.queryParams("phoneNumber"));

            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection passengerCol = db.getCollection("passengers");

            // update passenger by pid
            String pid = req.params(":pid");
            DBObject query = BasicDBObjectBuilder.start().add("pid", pid).get();
            Passenger passenger = new Passenger(req.queryParams("firstName"), req.queryParams("lastName"), req.queryParams("emailAddress"), req.queryParams("password"), req.queryParams("phoneNumber"));
            passenger.setPid(pid);
            DBObject doc = createDBObject(passenger);
            WriteResult result = passengerCol.update(query, doc);

            //close resources
            mongo.close();

            return passenger;
        }, new JsonTransformer());

        // delete a Passenger by id
        delete("/v1/passengers/:pid", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection passengerCol = db.getCollection("passengers");

            // delete passenger by id
            String pid = req.params(":pid");
            DBObject query = BasicDBObjectBuilder.start().add("pid", pid).get();
            WriteResult result = passengerCol.remove(query);

            //close resources
            mongo.close();

            return "delete success";
        }, new JsonTransformer());

        // guarantee that the response body is in Json formation
        after((req, res) -> {
            res.type("application/json");
        });

        // error handling
        exception(IllegalArgumentException.class, (e, req, res) -> {
            res.status(400);
            res.body(new ResponseError(e).getMessage());
        });
    }
    /**
     * Get DB reference by given Passenger information.
     */
    private static DBObject createDBObject(Passenger passenger) {
        BasicDBObjectBuilder docBuilder = BasicDBObjectBuilder.start();

        docBuilder.append("pid", passenger.getPid());
        docBuilder.append("firstName", passenger.getFirstName());
        docBuilder.append("lastName", passenger.getLastName());
        docBuilder.append("emailAddress", passenger.getEmailAddress());
        docBuilder.append("password", passenger.getPassword());
        docBuilder.append("phoneNumber", passenger.getPhoneNumber());
        return docBuilder.get();
    }
    /**
     * Query passenger by email.
     * */
    private List<Passenger> queryPassengerByEmail(DBCollection passengerCol,
                                                  String email) {
        DBObject query = BasicDBObjectBuilder.start().add("emailAddress",
                email).get();
        DBCursor cursor = passengerCol.find(query);
        List<Passenger> result = new ArrayList<Passenger>();
        while(cursor.hasNext()) {
            DBObject Obj = cursor.next();
            result.add(convertDBObject(Obj));
        }
        return result;
    }
    /**
     * Convert a Passenger DB record to Passenger object.
     */
    private static Passenger convertDBObject(DBObject Obj) {
        BasicDBObject passengerObj = (BasicDBObject) Obj;
        Passenger passenger = new Passenger(passengerObj.getString("firstName"),
                passengerObj.getString("lastName"),
                passengerObj.getString("emailAddress"),
                passengerObj.getString("password"),
                passengerObj.getString("phoneNumber"));
        passenger.setPassword("******"); // hide password
        passenger.setPid(passengerObj.getString("pid"));  //keep pid not changed
        return passenger;
    }

    /**
     * Convert a Ride DB record to Ride object.
     */
    private static Ride convertRideDBObject(DBObject Obj) {
        BasicDBObject rideObj = (BasicDBObject) Obj;
        Ride ride = new Ride(rideObj.getString("pid"), rideObj.getString("did"), rideObj.getString("rideType"), rideObj.getString("status"));
        ride.setRid(rideObj.getString("rid"));  //keep rid not changed
        return ride;
    }

    /**
     * Error Handling
     * Check the existence of given fields and raise error if any of them
     * does not exist or is invalid.
     */
    private void failIfInvalid(String firstName, String lastName, String emailAddress, String password, String phoneNumber) {
        if (firstName == null || firstName.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'firstName' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (lastName == null || lastName.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'lastName' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (emailAddress == null || emailAddress.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'emailAddress' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (password == null || password.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'password' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (phoneNumber == null || phoneNumber.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'phoneNumber' cannot be empty\n" +
                    "Status Code: 400");
        }
    }
}
