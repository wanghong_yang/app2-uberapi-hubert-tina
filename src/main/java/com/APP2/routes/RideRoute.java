package com.APP2.routes;

import com.mongodb.*;
import com.APP2.models.Ride;
import com.APP2.models.RoutePoint;
import com.APP2.JsonTransformer;
import com.APP2.ValidateToken;

import java.util.ArrayList;
import java.util.List;

import static spark.Spark.*;
import static spark.Spark.after;



/**
 * Controller class that install route of Ride related request handler.
 * It depends on the CRUD implementation of Ride Entity.
 */
public class RideRoute {
    public RideRoute() {
        // post a ride
        // Access Control: only user with correct token can access
        post("/v1/rides", (req, res) -> {
            // validate token
            String token = req.queryParams("token");
            String msg = ValidateToken.validateT(token);

            if (msg.equals("Driver") || msg.equals("Passenger")) {
                // Error Handling
                failIfInvalid(req.queryParams("pid"), req.queryParams("did"),
                        req.queryParams("rideType"), req.queryParams("status"));

                //open resources
                MongoClient mongo = new MongoClient("localhost", 27017);
                DB db = mongo.getDB("journaldev");
                DBCollection rideCol = db.getCollection("rides");

                // push a new Ride to database
                Ride ride = new Ride(req.queryParams("pid"),
                        req.queryParams("did"),
                        req.queryParams("rideType"), req.queryParams("status"));
                DBObject doc = createDBObject(ride);
                WriteResult result = rideCol.insert(doc);

                //close resources
                mongo.close();

                return ride;
            } else {
                return msg;
            }

        }, new JsonTransformer());

        // post a route point, POST /rides/:rideId/routePoints
        post("/v1/rides/:rid/routePoints", (req, res) -> {
            // Error Handling
            String rid = req.params(":rid");
            failIfRoutePointInvalid(rid, req.queryParams("lati"),
                    req.queryParams("longi"));

            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection RoutePointCol = db.getCollection("routePoints");

            // push a new route point to database
            RoutePoint routePoint = new RoutePoint(rid, req.queryParams("lati"), req.queryParams("longi"));
            DBObject doc = createRoutePointDBObject(routePoint);
            WriteResult result = RoutePointCol.insert(doc);

            //close resources
            mongo.close();

            return routePoint;
        }, new JsonTransformer());


        // get all route points, GET /rides/:rideId/routePoints
        get("/v1/rides/:rid/routePoints", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection RoutePointCol = db.getCollection("routePoints");

            // read route point by id
            List<RoutePoint> routePoints = new ArrayList<RoutePoint>();
            String rid = req.params(":rid");
            DBObject query = BasicDBObjectBuilder.start().add("rid", rid).get();
            DBCursor cursor = RoutePointCol.find(query);
            while(cursor.hasNext()) {
                DBObject Obj = cursor.next();
                routePoints.add(convertRoutePointDBObject(Obj));
            }

            //close resources
            mongo.close();

            return routePoints;
        }, new JsonTransformer());

        // get latest route point, GET /rides/:rideId/routePoints/latest
        get("/v1/rides/:rid/routePoints/latest", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection RoutePointCol = db.getCollection("routePoints");

            // read route point by id
            String rid = req.params(":rid");
            DBObject query = BasicDBObjectBuilder.start().add("rid", rid).get();
            DBObject order = new BasicDBObject("timeStamp", -1);
            DBCursor cursor = RoutePointCol.find(query).sort(order);
            //List<RoutePoint> routePoints = new ArrayList<RoutePoint>();

            try {
                RoutePoint routePoint = null;
                while (cursor.hasNext()) {
                    DBObject Obj = cursor.next();
                    //routePoints.add(convertRoutePointDBObject(Obj));
                    routePoint = convertRoutePointDBObject(Obj);
                }
                if (routePoint != null) {
                    return routePoint;
                }
            } finally {
                //close resources
                mongo.close();
            }

            // error handling: no route points with corresponding id found
            res.status(400);
            return new ResponseError("No route points with ride id '%s' found", rid);
        }, new JsonTransformer());

        // get a Ride by id
        get("/v1/rides/:rid", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection rideCol = db.getCollection("rides");

            // read ride by id
            String rid = req.params(":rid");
            try {
                DBObject query = BasicDBObjectBuilder.start().add("rid", rid).get();
                DBCursor cursor = rideCol.find(query);
                while (cursor.hasNext()) {
                    DBObject Obj = cursor.next();
                    return convertDBObject(Obj);
                }
            } finally {
                //close resources
                mongo.close();
            }

            // error handling: no ride with corresponding id found
            res.status(400);
            return new ResponseError("No Ride with ride id '%s' found", rid);
        }, new JsonTransformer());

        // get rides
        get("/v1/rides", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection rideCol = db.getCollection("rides");

            // read ride
            List<Ride> rides = new ArrayList<Ride>();
            DBCursor cursor = rideCol.find();
            while(cursor.hasNext()) {
                DBObject Obj = cursor.next();
                rides.add(convertDBObject(Obj));
            }

            //close resources
            mongo.close();

            return rides;
        }, new JsonTransformer());

        // update a ride
        put("/v1/rides/:rid", (req, res) -> {

            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection rideCol = db.getCollection("rides");

            // update ride by rid
            String rid = req.params(":rid");
            DBObject query = BasicDBObjectBuilder.start().add("rid", rid).get();
            Ride ride = new Ride(req.queryParams("pid"), req.queryParams("did"),
                    req.queryParams("rideType"), req.queryParams("status"));
            ride.setRid(rid);
            DBObject doc = createDBObject(ride);
            WriteResult result = rideCol.update(query, doc);

            //close resources
            mongo.close();

            return ride;
        }, new JsonTransformer());

        // delete a Ride by id
        delete("/v1/rides/:rid", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection rideCol = db.getCollection("rides");

            // delete ride by id
            String rid = req.params(":rid");
            DBObject query = BasicDBObjectBuilder.start().add("rid", rid).get();
            WriteResult result = rideCol.remove(query);

            //close resources
            mongo.close();

            return "delete success";
        }, new JsonTransformer());

        // guarantee that the response body is in Json formation
        after((req, res) -> {
            res.type("application/json");
        });

        // error handling
        exception(IllegalArgumentException.class, (e, req, res) -> {
            res.status(400);
            res.body(new ResponseError(e).getMessage());
        });
    }

    /**
     * Get DB reference by given Ride information.
     */
    private static DBObject createDBObject(Ride ride) {
        BasicDBObjectBuilder docBuilder = BasicDBObjectBuilder.start();

        docBuilder.append("rid", ride.getRid());
        docBuilder.append("pid", ride.getPid());
        docBuilder.append("did", ride.getDid());
        docBuilder.append("rideType", ride.getRideType());
        docBuilder.append("status", ride.getStatus());
        return docBuilder.get();
    }

    /**
     * Get DB reference by given RoutePoint information.
     */
    private static DBObject createRoutePointDBObject(RoutePoint routePoint) {
        BasicDBObjectBuilder docBuilder = BasicDBObjectBuilder.start();

        docBuilder.append("rid", routePoint.getRid());
        docBuilder.append("timeStamp", routePoint.getTimeStamp());
        docBuilder.append("lati", routePoint.getLati());
        docBuilder.append("longi", routePoint.getLongi());

        return docBuilder.get();
    }

    /**
     * Convert a Ride DB record to Ride object.
     */
    private static Ride convertDBObject(DBObject Obj) {
        BasicDBObject rideObj = (BasicDBObject) Obj;
        Ride ride = new Ride(rideObj.getString("pid"), rideObj.getString("did"), rideObj.getString("rideType"), rideObj.getString("status"));
        ride.setRid(rideObj.getString("rid"));  //keep rid not changed
        return ride;
    }

    /**
     * Convert a RoutePoint DB record to RoutePoint object.
     */
    private static RoutePoint convertRoutePointDBObject(DBObject Obj) {
        BasicDBObject routePointObj = (BasicDBObject) Obj;
        RoutePoint routePoint = new RoutePoint(routePointObj.getString("rid"), routePointObj.getString("lati"), routePointObj.getString("longi"));
        routePoint.setTimeStamp(routePointObj.getString("timeStamp"));  //keep timeStamp not changed
        return routePoint;
    }

    /**
     * Error Handling
     * Check the existence of given fields and raise error if any of them
     * does not exist or is invalid.
     */
    private void failIfInvalid(String pid, String did, String rideType, String status) {
        if (pid == null || pid.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'pid' passenger id cannot be empty\n" +
                    "Status Code: 400");
        }
        if (did == null || did.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'did' driver id cannot be empty\n" +
                    "Status Code: 400");
        }
        if (rideType == null || rideType.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'rideType' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (status == null || status.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'status' cannot be empty\n" +
                    "Status Code: 400");
        }
    }

    // Error Handling for route point
    private void failIfRoutePointInvalid(String rid, String lati, String longi) {
        if (rid == null || rid.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'rid' ride id cannot be empty\n" +
                    "Status Code: 400");
        }
        if (lati == null || lati.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'lati' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (longi == null || longi.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'longi' cannot be empty\n" +
                    "Status Code: 400");
        }
    }
}
