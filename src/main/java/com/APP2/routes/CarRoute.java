package com.APP2.routes;

import com.mongodb.*;
import com.APP2.models.Car;
import com.APP2.models.Driver;
import com.APP2.JsonTransformer;
import com.APP2.ValidateToken;

import java.util.ArrayList;
import java.util.List;

import static spark.Spark.*;
import static spark.Spark.after;

/**
 * Created by Hubert Wang on 11/9/16.
 *
 * Controller class that install route of Car related request handler.
 * It depends on the CRUD implementation of Car Entity.
 */
public class CarRoute {
    public CarRoute() {
        // post a Car, Access Control: only driver user with correct token can access
        post("/v1/cars", (req, res) -> {
            // validate token
            String token = req.queryParams("token");
            String msg = ValidateToken.validateT(token);
            System.out.println(msg);

            if (msg.equals("Driver")) {
                // Error Handling
                failIfInvalid(req.queryParams("did"), req.queryParams("make"),
                        req.queryParams("model"), req.queryParams("license"),
                        req.queryParams("doorCount"));

                //open resources
                MongoClient mongo = new MongoClient("localhost", 27017);
                DB db = mongo.getDB("journaldev");
                DBCollection carCol = db.getCollection("cars");

                // push a new Car to database
                Car car = new Car(req.queryParams("did"), req.queryParams("make"), req.queryParams("model"), req.queryParams("license"), req.queryParams("doorCount"));
                DBObject doc = createDBObject(car);
                WriteResult result = carCol.insert(doc);

                //close resources
                mongo.close();

                return car;
            } else if (msg.equals("Passenger")) {
                return "Post car illegal for non-driver users.";
            } else {
                return msg;
            }

        }, new JsonTransformer());

        // get a Car by id
        get("/v1/cars/:cid", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection carCol = db.getCollection("cars");

            // read car by id
            String cid = req.params(":cid");
            DBObject query = BasicDBObjectBuilder.start().add("cid", cid).get();
            DBCursor cursor = carCol.find(query);
            while (cursor.hasNext()) {
                DBObject Obj = cursor.next();
                return convertDBObject(Obj);
            }

            //close resources
            mongo.close();

            // error handling: no car with corresponding id found
            res.status(400);
            return new ResponseError("No Car with cid '%s' found", cid);
        }, new JsonTransformer());

        // GET /cars/:carId/drivers, get driver by car ID
        get("/v1/cars/:cid/drivers", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection carCol = db.getCollection("cars");
            DBCollection DriverCol = db.getCollection("drivers");

            // read driver by id
            String cid = req.params(":cid");
            DBObject query = BasicDBObjectBuilder.start().add("cid", cid).get();
            DBCursor cursor = carCol.find(query);
            String did = "";
            while(cursor.hasNext()) {
                DBObject Obj = cursor.next();
                BasicDBObject carObj = (BasicDBObject) Obj;
                did = carObj.getString("did");
            }

            query = BasicDBObjectBuilder.start().add("did", did).get();
            cursor = DriverCol.find(query);
            while(cursor.hasNext()) {
                DBObject Obj = cursor.next();
                return convertDriverDBObject(Obj);
            }

            //close resources
            mongo.close();

            // error handling: no Driver with corresponding car id found
            res.status(400);
            return new ResponseError("No Driver with car id '%s' found", cid);
        }, new JsonTransformer());

        // get cars
        get("/v1/cars", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection carCol = db.getCollection("cars");

            // read cars
            List<Car> cars = new ArrayList<Car>();
            DBCursor cursor = carCol.find();
            while(cursor.hasNext()) {
                DBObject Obj = cursor.next();
                cars.add(convertDBObject(Obj));
            }

            //close resources
            mongo.close();

            return cars;
        }, new JsonTransformer());

        // update a car
        put("/v1/cars/:cid", (req, res) -> {
            // Error Handling
            failIfInvalid(req.queryParams("did"), req.queryParams("make"), req.queryParams("model"), req.queryParams("license"), req.queryParams("doorCount"));

            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection carCol = db.getCollection("cars");

            // update car by cid
            String cid = req.params(":cid");
            DBObject query = BasicDBObjectBuilder.start().add("cid", cid).get();
            Car car = new Car(req.queryParams("did"), req.queryParams("make"), req.queryParams("model"), req.queryParams("license"), req.queryParams("doorCount"));
            car.setCid(cid);
            DBObject doc = createDBObject(car);
            WriteResult result = carCol.update(query, doc);

            //close resources
            mongo.close();

            return car;
        }, new JsonTransformer());

        // delete a Car by id
        delete("/v1/cars/:cid", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection carCol = db.getCollection("cars");

            // delete car by id
            String cid = req.params(":cid");
            DBObject query = BasicDBObjectBuilder.start().add("cid", cid).get();
            WriteResult result = carCol.remove(query);

            //close resources
            mongo.close();

            return "delete success";
        }, new JsonTransformer());

        // guarantee that the response body is in Json formation
        after((req, res) -> {
            res.type("application/json");
        });

        // error handling
        exception(IllegalArgumentException.class, (e, req, res) -> {
            res.status(400);
            res.body(new ResponseError(e).getMessage());
        });
    }

    /**
     * Get DB reference by given Car information.
     */
    private static DBObject createDBObject(Car car) {
        BasicDBObjectBuilder docBuilder = BasicDBObjectBuilder.start();

        docBuilder.append("cid", car.getCid());
        docBuilder.append("did", car.getDid());
        docBuilder.append("make", car.getMake());
        docBuilder.append("model", car.getModel());
        docBuilder.append("license", car.getLicense());
        docBuilder.append("doorCount", car.getDoorCount());
        return docBuilder.get();
    }

    /**
     * Convert a Car DB record to Car object.
     */
    private static Car convertDBObject(DBObject Obj) {
        BasicDBObject carObj = (BasicDBObject) Obj;
        Car car = new Car(carObj.getString("did"),
                carObj.getString("make"),
                carObj.getString("model"),
                carObj.getString("license"),
                carObj.getString("doorCount"));
        car.setCid(carObj.getString("cid"));  //keep cid not changed
        return car;
    }


    /**
     * Convert a Driver DB record to Driver object.
     */
    private static Driver convertDriverDBObject(DBObject Obj) {
        BasicDBObject driverObj = (BasicDBObject) Obj;
        Driver driver = new Driver(driverObj.getString("firstName"), driverObj.getString("lastName"), driverObj.getString("emailAddress"), driverObj.getString("password"));
        driver.setDid(driverObj.getString("did"));  //keep did not changed
        return driver;
    }


    /**
     * Error Handling
     * Check the existence of given fields and raise error if any of them
     * does not exist or is invalid.
     */
    private void failIfInvalid(String did, String make, String model,
                               String license, String doorCount) {
        if (did == null || did.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'did' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (make == null || make.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'make' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (model == null || model.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'model' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (license == null || license.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'license' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (doorCount == null || doorCount.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'doorCount' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (make.length() >= 18) {
            throw new IllegalArgumentException("Error Code: 1002\n" +
                    "Parameter 'make' length limit: <18\n" +
                    "Status Code: 400");
        }
        if (model.length() >= 18) {
            throw new IllegalArgumentException("Error Code: 1002\n" +
                    "Parameter 'model' length limit: <18\n" +
                    "Status Code: 400");
        }
        if (license.length() >= 10) {
            throw new IllegalArgumentException("Error Code: 1002\n" +
                    "Parameter 'license' length limit: <10\n" +
                    "Status Code: 400");
        }
        if (Integer.parseInt(doorCount) <= 1 ||
                Integer.parseInt(doorCount) >= 8) {
            throw new IllegalArgumentException("Error Code: 1002\n" +
                    "Parameter 'doorCount' limit: 1~8 \n" +
                    "Status Code: 400");
        }
    }
}
