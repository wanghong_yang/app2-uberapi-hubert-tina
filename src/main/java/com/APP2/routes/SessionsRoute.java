package com.APP2.routes;

import com.mongodb.*;
import com.APP2.JsonTransformer;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Date;

import static spark.Spark.post;

/*
* Controller class that install route of session management.
* */
public class SessionsRoute {
    public SessionsRoute() {
        // post email and password to get token lasting for 30 minutes
        post("/v1/sessions", (req, res) -> {
            // String email = "aaa@aaa.com";
            // String passWord = "aaa";
            String email = req.queryParams("emailAddress");
            String password = req.queryParams("password");
            failIfInvalid(email, password);

            // find correct password from DB
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection driverCol = db.getCollection("drivers");
            DBCollection passengerCol = db.getCollection("passengers");

            // find driver/passenger by email, and get correct password
            String correctPassword = "";
            boolean ifFound = false;
            String userType = "null"; // User Type: "Driver", "Passenger", "null"
            DBObject query = BasicDBObjectBuilder.start().add("emailAddress", email).get();
            DBCursor cursor = driverCol.find(query);
            while(cursor.hasNext()) {
                ifFound = true;
                userType = "Driver";
                DBObject Obj = cursor.next();
                BasicDBObject driverObj = (BasicDBObject) Obj;
                correctPassword = driverObj.getString("password");
            }
            DBCursor cursor2 = passengerCol.find(query);
            while(cursor2.hasNext()) {
                ifFound = true;
                userType = "Passenger";
                DBObject Obj = cursor2.next();
                BasicDBObject passengerObj = (BasicDBObject) Obj;
                correctPassword = passengerObj.getString("password");
            }

            //close resources
            mongo.close();


            if(!ifFound) {
                return "no corresponding user with the email found!";
            } else if (!correctPassword.equals(password)) {
                return "password is no correct, please check and input again.";
            } else {
                String expiration = Integer.toString((int)(new Date().getTime()/1000) + 3600);  // expiration: 60 minutes

                String clearString = email + ":" + expiration;

                // HmacSHA256
                String hashKey = "CMU";
                Mac mac = Mac.getInstance("HmacSHA256");
                SecretKeySpec secretKey = new SecretKeySpec(hashKey.getBytes("UTF-8"), "HmacSHA256");
                mac.init(secretKey);
                byte[] hmacData = mac.doFinal(clearString.getBytes("UTF-8"));
                String hashString = hmacData.toString();

                // encryption
                String IV = "AAAAAAAAAAAAAAAA";
                String encryptionKey = "0123456789abcdef";
                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
                SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
                cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
                String wholeString = clearString + ":" +hashString + ":" + userType;
                byte[] cipherData = cipher.doFinal(wholeString.getBytes("UTF-8"));

                // get token
                String token = Base64.getEncoder().encodeToString(cipherData);

                return token;
            }
        }, new JsonTransformer());
    }
    private void failIfInvalid(String emailAddress, String password) {
        if (emailAddress == null || emailAddress.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'emailAddress' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (password == null || password.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'password' cannot be empty\n" +
                    "Status Code: 400");
        }
    }
}
