package com.APP2.routes;

import com.mongodb.*;
import com.APP2.models.Car;
import com.APP2.models.Driver;
import com.APP2.models.Ride;
import com.APP2.JsonTransformer;
import com.APP2.ValidateToken;

import java.util.ArrayList;
import java.util.List;

import static spark.Spark.*;
import static spark.Spark.after;
import static spark.Spark.exception;

/**
 * Controller class that install route of Driver related request handler.
 * It depends on the CRUD implementation of Driver Entity.
 */
public class DriverRoute {
    public DriverRoute() {
        // post a Driver
        post("/v1/drivers", (req, res) -> {
            // Error Handling
            failIfInvalid(req.queryParams("firstName"),
                          req.queryParams("lastName"),
                          req.queryParams("emailAddress"),
                          req.queryParams("password"));

            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection driverCol = db.getCollection("drivers");

            // Check if the driver is exist.
            List<Driver> existDrivers = queryDriverByEmail
                    (driverCol, req.queryParams("emailAddress"));
            if (existDrivers.size() > 0) {
                throw new IllegalArgumentException("Error Code : 1100\n"
                        + "Error Msg: Parameter 'emailAddress' already exist." +
                        " \n" + "Status Code: 400"
                );
            }

            // push a new Driver to database
            Driver driver = new Driver(req.queryParams("firstName"), req.queryParams("lastName"), req.queryParams("emailAddress"), req.queryParams("password"));
            DBObject doc = createDBObject(driver);
            WriteResult result = driverCol.insert(doc);

            //close resources
            mongo.close();

            return driver;
        }, new JsonTransformer());

        // POST /drivers/:driverId/cars, post a Car by driver ID
        // Access Control: only driver user with correct token can access
        post("/v1/drivers/:did/cars", (req, res) -> {
            // validate token
            String token = req.queryParams("token");
            String msg = ValidateToken.validateT(token);

            if (msg.equals("Driver")) {
                // Error Handling
                String did = req.params(":did");
                failIfCarInvalid(did, req.queryParams("make"), req.queryParams("model"), req.queryParams("license"), req.queryParams("doorCount"));

                //open resources
                MongoClient mongo = new MongoClient("localhost", 27017);
                DB db = mongo.getDB("journaldev");
                DBCollection CarCol = db.getCollection("cars");

                // push a new Car to database
                Car car = new Car(did, req.queryParams("make"), req.queryParams("model"), req.queryParams("license"), req.queryParams("doorCount"));
                DBObject doc = createCarDBObject(car);
                WriteResult result = CarCol.insert(doc);

                //close resources
                mongo.close();

                return car;
            } else if (msg.equals("Passenger")) {
                return "Post car illegal for non-driver users.";
            } else {
                return msg;
            }

        }, new JsonTransformer());

        // get a Driver by id
        get("/v1/drivers/:did", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection driverCol = db.getCollection("drivers");

            // read driver by id
            String did = req.params(":did");
            DBObject query = BasicDBObjectBuilder.start().add("did", did).get();
            DBCursor cursor = driverCol.find(query);
            while(cursor.hasNext()) {
                DBObject Obj = cursor.next();
                return convertDBObject(Obj);
            }

            //close resources
            mongo.close();

            // error handling: no driver with corresponding id found
            res.status(400);
            return new ResponseError("No Driver with did '%s' found", did);
        }, new JsonTransformer());

        // get drivers
        get("/v1/drivers", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection driverCol = db.getCollection("drivers");

            // read drivers
            List<Driver> drivers = new ArrayList<Driver>();
            DBCursor cursor = driverCol.find();
            while(cursor.hasNext()) {
                DBObject Obj = cursor.next();
                drivers.add(convertDBObject(Obj));
            }

            //close resources
            mongo.close();

            return drivers;
        }, new JsonTransformer());

        //GET /drivers/:driverId/cars, get drivers's cars by driver id
        get("/v1/drivers/:did/cars", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection CarCol = db.getCollection("cars");

            // read driver by id
            List<Car> cars = new ArrayList<Car>();
            String did = req.params(":did");
            DBObject query = BasicDBObjectBuilder.start().add("did", did).get();
            DBCursor cursor = CarCol.find(query);
            while(cursor.hasNext()) {
                DBObject Obj = cursor.next();
                cars.add(convertCarDBObject(Obj));
            }

            //close resources
            mongo.close();

            return cars;
        }, new JsonTransformer());

        // GET /drivers/:driverId/rides, get drivers's rides by driver id
        get("/v1/drivers/:did/rides", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection RideCol = db.getCollection("rides");

            // read driver by id
            List<Ride> rides = new ArrayList<Ride>();
            String did = req.params(":did");
            DBObject query = BasicDBObjectBuilder.start().add("did", did).get();
            DBCursor cursor = RideCol.find(query);
            while(cursor.hasNext()) {
                DBObject Obj = cursor.next();
                rides.add(convertRideDBObject(Obj));
            }

            //close resources
            mongo.close();

            return rides;
        }, new JsonTransformer());

        // update a driver
        put("/v1/drivers/:did", (req, res) -> {
            // Error Handling
            failIfInvalid(req.queryParams("firstName"), req.queryParams("lastName"), req.queryParams("emailAddress"), req.queryParams("password"));

            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection driverCol = db.getCollection("drivers");

            // update driver by did
            String did = req.params(":did");
            DBObject query = BasicDBObjectBuilder.start().add("did", did).get();
            Driver driver = new Driver(req.queryParams("firstName"), req.queryParams("lastName"), req.queryParams("emailAddress"), req.queryParams("password"));
            driver.setDid(did);
            DBObject doc = createDBObject(driver);
            WriteResult result = driverCol.update(query, doc);

            //close resources
            mongo.close();

            return driver;
        }, new JsonTransformer());

        // delete a Driver by id
        delete("/v1/drivers/:did", (req, res) -> {
            //open resources
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("journaldev");
            DBCollection driverCol = db.getCollection("drivers");

            // delete driver by id
            String did = req.params(":did");
            DBObject query = BasicDBObjectBuilder.start().add("did", did).get();
            WriteResult result = driverCol.remove(query);

            //close resources
            mongo.close();

            return "delete success";
        }, new JsonTransformer());

        // guarantee that the response body is in Json formation
        after((req, res) -> {
            res.type("application/json");
        });

        // error handling
        exception(IllegalArgumentException.class, (e, req, res) -> {
            res.status(400);
            res.body(new ResponseError(e).getMessage());
        });
    }

    /**
     * Get DB reference by given Driver information.
     */
    private static DBObject createDBObject(Driver driver) {
        BasicDBObjectBuilder docBuilder = BasicDBObjectBuilder.start();

        docBuilder.append("did", driver.getDid());
        docBuilder.append("firstName", driver.getFirstName());
        docBuilder.append("lastName", driver.getLastName());
        docBuilder.append("emailAddress", driver.getEmailAddress());
        docBuilder.append("password", driver.getPassword());
        return docBuilder.get();
    }
    /**
     * Get DB reference by given Car information.
     */
    private static DBObject createCarDBObject(Car car) {
        BasicDBObjectBuilder docBuilder = BasicDBObjectBuilder.start();

        docBuilder.append("cid", car.getCid());
        docBuilder.append("did", car.getDid());
        docBuilder.append("make", car.getMake());
        docBuilder.append("model", car.getModel());
        docBuilder.append("license", car.getLicense());
        docBuilder.append("doorCount", car.getDoorCount());
        return docBuilder.get();
    }

    /*
    * Query driver by email.
    * */
    private  List<Driver> queryDriverByEmail(DBCollection driverCol, String
            email){
        DBObject query = BasicDBObjectBuilder.start().add("emailAddress",
                email).get();
        DBCursor cursor = driverCol.find(query);
        List<Driver> result = new ArrayList<Driver>();
        while(cursor.hasNext()){
            DBObject Obj = cursor.next();
            result.add(convertDBObject(Obj));
        }
        return result;

    }

    /**
     * Convert a Driver DB record to Driver object.
     */
    private static Driver convertDBObject(DBObject Obj) {
        BasicDBObject driverObj = (BasicDBObject) Obj;
        Driver driver = new Driver(driverObj.getString("firstName"), driverObj.getString("lastName"), driverObj.getString("emailAddress"), driverObj.getString("password")); 
        driver.setPassword("******"); // hide password
        driver.setDid(driverObj.getString("did"));  //keep did not changed
        return driver;
    }

    /**
     * Convert a Car DB record to Car object.
     */
    private static Car convertCarDBObject(DBObject Obj) {
        BasicDBObject carObj = (BasicDBObject) Obj;
        Car car = new Car(carObj.getString("did"), carObj.getString("make"), carObj.getString("model"), carObj.getString("license"), carObj.getString("doorCount"));
        car.setCid(carObj.getString("cid"));  //keep cid not changed
        return car;
    }

    /**
     *Convert a Ride DB record to Ride object.
     */
    private static Ride convertRideDBObject(DBObject Obj) {
        BasicDBObject rideObj = (BasicDBObject) Obj;
        Ride ride = new Ride(rideObj.getString("pid"), rideObj.getString("did"), rideObj.getString("rideType"), rideObj.getString("status"));
        ride.setRid(rideObj.getString("rid"));  //keep rid not changed
        return ride;
    }

    /**
     * Error Handling
     * Check the existence of given fields and raise error if any of them
     * does not exist or is invalid.
     */
    private void failIfInvalid(String firstName, String lastName,
                               String emailAddress, String password) {
        if (firstName == null || firstName.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'firstName' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (lastName == null || lastName.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'lastName' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (emailAddress == null || emailAddress.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'emailAddress' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (password == null || password.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'password' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (firstName.length() >= 18) {
            throw new IllegalArgumentException("Error Code: 1002\n" +
                    "Parameter 'firstName' length limit: <18\n" +
                    "Status Code: 400");
        }
        if (lastName.length() >= 18) {
            throw new IllegalArgumentException("Error Code: 1002\n" +
                    "Parameter 'lastName' length limit: <18\n" +
                    "Status Code: 400");
        }
    }


    /**
     * Error Handling for Car
     * Check the existence of given fields and raise error if any of them
     * does not exist or is invalid.
     */
    private void failIfCarInvalid(String did, String make, String model, String license, String doorCount) {
        if (did == null || did.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'did' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (make == null || make.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'make' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (model == null || model.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'model' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (license == null || license.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'license' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (doorCount == null || doorCount.isEmpty()) {
            throw new IllegalArgumentException("Error Code: 1001\n" +
                    "Error Msg: Parameter 'doorCount' cannot be empty\n" +
                    "Status Code: 400");
        }
        if (make.length() >= 18) {
            throw new IllegalArgumentException("Error Code: 1002\n" +
                    "Parameter 'make' length limit: <18\n" +
                    "Status Code: 400");
        }
        if (model.length() >= 18) {
            throw new IllegalArgumentException("Error Code: 1002\n" +
                    "Parameter 'model' length limit: <18\n" +
                    "Status Code: 400");
        }
        if (license.length() >= 10) {
            throw new IllegalArgumentException("Error Code: 1002\n" +
                    "Parameter 'license' length limit: <10\n" +
                    "Status Code: 400");
        }
        if (Integer.parseInt(doorCount) <=1 || Integer.parseInt(doorCount) >= 8) {
            throw new IllegalArgumentException("Error Code: 1002\n" +
                    "Parameter 'doorCount' limit: 1~8 \n" +
                    "Status Code: 400");
        }
    }
}
