package com.APP2;

/**
 * Created by Hubert Wang & Tina Cai on 2016/10/25.
 */

import com.APP2.routes.*;
import java.net.UnknownHostException;

public class Main {
    public static void main(String[] args) throws UnknownHostException {
        new SessionsRoute();

        new CarRoute();
        new DriverRoute();
        new RideRoute();
        new PassengerRoute();
    }
}
