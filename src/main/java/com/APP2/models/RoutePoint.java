package com.APP2.models;

import java.util.Date;

/**
 * Created by Hubert on 11/10/16.
 */

/**
 * Definition of RideRoute entity.
 */
public class RoutePoint {
    private String rid;
    private String timeStamp;
    private String lati;
    private String longi;

    public RoutePoint(String rid, String lati, String longi) {
        this.rid = rid;
        this.timeStamp = Integer.toString((int)(new Date().getTime()/1000));
        this.lati = lati;
        this.longi = longi;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getLati() {
        return lati;
    }

    public void setLati(String lati) {
        this.lati = lati;
    }

    public String getLongi() {
        return longi;
    }

    public void setLongi(String longi) {
        this.longi = longi;
    }


}
