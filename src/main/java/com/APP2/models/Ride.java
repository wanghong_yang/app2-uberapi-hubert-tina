package com.APP2.models;

import java.util.UUID;

/**
 * Created by Shaojie on 11/1/16.
 */


/**
 *
 * Parameters constraints
 * - passenger (reference)
 * - driver (reference)
 * - car (reference)
 * - rideType (String, [ECONOMY, PREMIUM, EXECUTIVE], Required)
 * - startPoint  Object (lat: Number, long: Number, Required)
 * - endPoint Object (lat: Number, long: Number, Required)
 * - requestTime (Number, TimeStamp, Required)
 * - pickupTime (Number, TimeStamp, Required)
 * - dropOffTime (Number, TimeStamp, Required)
 * - status (String, [REQUESTED, AWAITING_DRIVER, DRIVE_ASSIGNED, IN_PROGRESS, ARRIVED, CLOSED], Required)
 * - fare (Number)
 * - route (series of latitude/longitude values)
 */

/**
 * Definition of Ride entity.
 */
public class Ride {

    private String rid;
    private String pid;
    private String did;
    private String rideType;
    private Location startPoint;
    private Location endPoint;
    private String requestTime;
    private String pickupTime;
    private String dropOffTime;
    private String status;

    public Ride(String pid, String did, String rideType, String status) {
        this.rid = UUID.randomUUID().toString();
        this.pid = pid;
        this.did = did;
        this.rideType = rideType;
        this.status = status;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getRideType() {
        return rideType;
    }

    public void setRideType(String rideType) {
        this.rideType = rideType;
    }

    public Location getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(Location startPoint) {
        this.startPoint = startPoint;
    }

    public Location getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(Location endPoint) {
        this.endPoint = endPoint;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getDropOffTime() {
        return dropOffTime;
    }

    public void setDropOffTime(String dropOffTime) {
        this.dropOffTime = dropOffTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public class Location {
        private double lat;
        private double lng;

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }

}


