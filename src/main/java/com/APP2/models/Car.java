package com.APP2.models;

import java.util.UUID;

/**
 * Created by Hubert Wang on 11/9/16.
 *
 * Definition of Car entity.
 * */
public class Car {
    private String cid;
    private String did;  // driver id, bound to driver
    private String make;
    private String model;
    private String license;
    private String doorCount;
    private String carType;
    private int maxPassengers;
    private String color;
    private String validRideTypes;

    public Car(String did, String make, String model, String license,
               String doorCount) {
        this.cid = UUID.randomUUID().toString();
        this.did = did;
        this.make = make;
        this.model = model;
        this.license = license;
        this.doorCount = doorCount;
    }


    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getDoorCount() {
        return doorCount;
    }

    public void setDoorCount(String doorCount) {
        this.doorCount = doorCount;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public int getMaxPassengers() {
        return maxPassengers;
    }

    public void setMaxPassengers(int maxPassengers) {
        this.maxPassengers = maxPassengers;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getValidRideTypes() {
        return validRideTypes;
    }

    public void setValidRideTypes(String validRideTypes) {
        this.validRideTypes = validRideTypes;
    }

}
