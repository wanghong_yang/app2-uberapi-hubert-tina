package com.APP2.models;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.UUID;

/**
 * Created by Shaojie on 11/1/16.
 */


/**
 * Parameters constraints
 * firstName (String, 1-15)
 * lastName (String, 1-15)
 * emailAddress (Reegex /[a-zA-Z0-9_.]+\@[a-zA-Z](([a-zA-Z0-9-]+).)*\/ , Required)
 * password (Used for POST only, String, 8-16, Required - No constraints, Store clear text)
 * addressLine1 (String, 50)
 * addressLine2 (String, 50)
 * city (String, 50)
 * state (String, 2)
 * zip (String, 5)
 * phoneNumber (String, Regex XXX-XXX-XXXX, Required)
 *
 * Definition of Passenger entity.
 */
 @JsonIgnoreProperties({"password"})
public class Passenger {

    private String pid;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String password;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String state;
    private String zip;
    private String phoneNumber;

    public Passenger(String firstName, String lastName, String emailAddress, String password, String phoneNumber) {
        this.pid = UUID.randomUUID().toString();
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.password = password;
        this.phoneNumber = phoneNumber;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


}



