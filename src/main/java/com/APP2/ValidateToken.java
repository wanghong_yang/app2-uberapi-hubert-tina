package com.APP2;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Date;

/**
 * Created by Hubert Wang on 11/16/16.
 * function: decode provided token and return usertype or error msg
 *           the decode process is corresponding to the SessionsRoute.java
 * Usage: ValidateToken.validateT(tokenString)
 * Para: tokenString
 * return: String of usertype (Driver/Passenger) or Error Msg
 */
public class ValidateToken {
    public static String validateT(String tokenString) {
        String token = tokenString; // req.queryParams("token");
        if(token == null || token.isEmpty()) {return "please provide token";}
        byte[] cipherData = Base64.getDecoder().decode(token);

        try {
            // decryption
            String IV = "AAAAAAAAAAAAAAAA";
            String encryptionKey = "0123456789abcdef";
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
            SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
            String wholeString = new String(cipher.doFinal(cipherData), "UTF-8");
            String[] strings = wholeString.split("\\:");
            String email = strings[0];
            String expiration = strings[1];
            String clearString = email + ":" + expiration;
            String hashString = strings[2];
            String userType = strings[3]; // User Type: "Driver", "Passenger", "null"

            // HmacSHA256
            String hashKey = "CMU";
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKey = new SecretKeySpec(hashKey.getBytes("UTF-8"), "HmacSHA256");
            mac.init(secretKey);
            byte[] hmacData = mac.doFinal(clearString.getBytes("UTF-8"));
            String reHashedString = hmacData.toString();

            if (hashString.equals(reHashedString)) {
                return "token wrong";
            } else if (Integer.parseInt(expiration) < (int) (new Date().getTime() / 1000)) {
                return "token expired!";
            } else if (userType.equals("Driver")) {
                return "Driver";  // Success
            } else {
                return "Passenger"; // Success
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Wrong token.");
        }
    }
}
